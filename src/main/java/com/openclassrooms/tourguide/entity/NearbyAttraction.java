package com.openclassrooms.tourguide.entity;

import gpsUtil.location.Location;

public class NearbyAttraction {
  public final String attractionName;
  public final Location attractionLocation;
  public final Location userLocation;
  public final double distance;
  public final int rewardPoint;

  public NearbyAttraction(String attractionName, Location attractionLocation, Location userLocation, double distance, int rewardPoint) {
    this.attractionName = attractionName;
    this.attractionLocation = attractionLocation;
    this.userLocation = userLocation;
    this.distance = distance;
    this.rewardPoint = rewardPoint;
  }
}
